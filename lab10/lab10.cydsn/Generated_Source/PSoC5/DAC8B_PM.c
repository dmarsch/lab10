/*******************************************************************************
* File Name: DAC8B_PM.c  
* Version 1.90
*
* Description:
*  This file provides the power management source code to API for the
*  VDAC8.  
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "DAC8B.h"

static DAC8B_backupStruct DAC8B_backup;


/*******************************************************************************
* Function Name: DAC8B_SaveConfig
********************************************************************************
* Summary:
*  Save the current user configuration
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void DAC8B_SaveConfig(void) 
{
    if (!((DAC8B_CR1 & DAC8B_SRC_MASK) == DAC8B_SRC_UDB))
    {
        DAC8B_backup.data_value = DAC8B_Data;
    }
}


/*******************************************************************************
* Function Name: DAC8B_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
*******************************************************************************/
void DAC8B_RestoreConfig(void) 
{
    if (!((DAC8B_CR1 & DAC8B_SRC_MASK) == DAC8B_SRC_UDB))
    {
        if((DAC8B_Strobe & DAC8B_STRB_MASK) == DAC8B_STRB_EN)
        {
            DAC8B_Strobe &= (uint8)(~DAC8B_STRB_MASK);
            DAC8B_Data = DAC8B_backup.data_value;
            DAC8B_Strobe |= DAC8B_STRB_EN;
        }
        else
        {
            DAC8B_Data = DAC8B_backup.data_value;
        }
    }
}


/*******************************************************************************
* Function Name: DAC8B_Sleep
********************************************************************************
* Summary:
*  Stop and Save the user configuration
*
* Parameters:  
*  void:  
*
* Return: 
*  void
*
* Global variables:
*  DAC8B_backup.enableState:  Is modified depending on the enable 
*  state  of the block before entering sleep mode.
*
*******************************************************************************/
void DAC8B_Sleep(void) 
{
    /* Save VDAC8's enable state */    
    if(DAC8B_ACT_PWR_EN == (DAC8B_PWRMGR & DAC8B_ACT_PWR_EN))
    {
        /* VDAC8 is enabled */
        DAC8B_backup.enableState = 1u;
    }
    else
    {
        /* VDAC8 is disabled */
        DAC8B_backup.enableState = 0u;
    }
    
    DAC8B_Stop();
    DAC8B_SaveConfig();
}


/*******************************************************************************
* Function Name: DAC8B_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DAC8B_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void DAC8B_Wakeup(void) 
{
    DAC8B_RestoreConfig();
    
    if(DAC8B_backup.enableState == 1u)
    {
        /* Enable VDAC8's operation */
        DAC8B_Enable();

        /* Restore the data register */
        DAC8B_SetValue(DAC8B_Data);
    } /* Do nothing if VDAC8 was disabled before */    
}


/* [] END OF FILE */
