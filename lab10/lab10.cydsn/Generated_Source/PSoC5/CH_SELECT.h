/*******************************************************************************
* File Name: CH_SELECT.h
* Version 1.80
*
*  Description:
*    This file contains the constants and function prototypes for the Analog
*    Multiplexer User Module AMux.
*
*   Note:
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_AMUX_CH_SELECT_H)
#define CY_AMUX_CH_SELECT_H

#include "cyfitter.h"
#include "cyfitter_cfg.h"

#if ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC4) || \
         (CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC5))    
    #include "cytypes.h"
#else
    #include "syslib/cy_syslib.h"
#endif /* ((CYDEV_CHIP_FAMILY_USED == CYDEV_CHIP_FAMILY_PSOC3) */


/***************************************
*        Function Prototypes
***************************************/

void CH_SELECT_Start(void) ;
#define CH_SELECT_Init() CH_SELECT_Start()
void CH_SELECT_FastSelect(uint8 channel) ;
/* The Stop, Select, Connect, Disconnect and DisconnectAll functions are declared elsewhere */
/* void CH_SELECT_Stop(void); */
/* void CH_SELECT_Select(uint8 channel); */
/* void CH_SELECT_Connect(uint8 channel); */
/* void CH_SELECT_Disconnect(uint8 channel); */
/* void CH_SELECT_DisconnectAll(void) */


/***************************************
*         Parameter Constants
***************************************/

#define CH_SELECT_CHANNELS  2u
#define CH_SELECT_MUXTYPE   1
#define CH_SELECT_ATMOSTONE 1

/***************************************
*             API Constants
***************************************/

#define CH_SELECT_NULL_CHANNEL 0xFFu
#define CH_SELECT_MUX_SINGLE   1
#define CH_SELECT_MUX_DIFF     2


/***************************************
*        Conditional Functions
***************************************/

#if CH_SELECT_MUXTYPE == CH_SELECT_MUX_SINGLE
# if !CH_SELECT_ATMOSTONE
#  define CH_SELECT_Connect(channel) CH_SELECT_Set(channel)
# endif
# define CH_SELECT_Disconnect(channel) CH_SELECT_Unset(channel)
#else
# if !CH_SELECT_ATMOSTONE
void CH_SELECT_Connect(uint8 channel) ;
# endif
void CH_SELECT_Disconnect(uint8 channel) ;
#endif

#if CH_SELECT_ATMOSTONE
# define CH_SELECT_Stop() CH_SELECT_DisconnectAll()
# define CH_SELECT_Select(channel) CH_SELECT_FastSelect(channel)
void CH_SELECT_DisconnectAll(void) ;
#else
# define CH_SELECT_Stop() CH_SELECT_Start()
void CH_SELECT_Select(uint8 channel) ;
# define CH_SELECT_DisconnectAll() CH_SELECT_Start()
#endif

#endif /* CY_AMUX_CH_SELECT_H */


/* [] END OF FILE */
