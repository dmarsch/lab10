/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>

#define CLOCK_SPEED         24000000
#define CLOCKS_PER_SAMPLE   16

#define RATE_1              50000
#define RATE_2              75000
#define RATE_3              100000
#define RATE_4              125000
#define RATE_5              150000

#define THOUSANDS_PLACE     1000
#define HUNDREDS_PLACE      100
#define TENS_PLACE          10
#define ERROR               9000

#define NUM_BYTES           8195

extern volatile uint8 flag;
char userInput = 0;
uint8 enter_flag = 0;
char disp[5];
uint16 address_a = 0;
uint16 address_b = 0;
uint8 ram[8192];
uint8 channel_a[8192];
uint8 channel_b[8192];
uint16 trigger = 2048;

void hacked_SPIM_PutArray(const uint8 buffer[], uint16 byteCount)   //function used to make SPIM reads easier by using sequential mode on the SRAM                                                               
{
    uint16 bufIndex;

    bufIndex = 0u;

    while(byteCount > 0u)
    {
        SPIM_WriteTxData(buffer[bufIndex]);
        bufIndex++;
        byteCount--;
    }
}

char blocking_char()                                        //when input != 0 function exits and returns a char
{
    char input = 0;
    
    while(input == 0) input = UART_GetChar();
    return input;
}

void save(char* inputArray, uint8 num_chars)                //inputArray global
{
    int i = 0;
    while(i < 100)
    {
        char in = blocking_char();
        if (in != 0x0D)                                     //if not carriage return
        { 
            if (((in == 0x08) || (in == 0x7f)) && i >= 1)   //if BS or DEL pressed, rubout sequence
            {
                UART_PutChar(0x08);                         //rubout sequence to realterm
                UART_PutChar(0x20);
                UART_PutChar(0x08);
                i = i -1;                                   //move back 1 and write over the BS or DEL
                inputArray[i] = '\0';
            }
            else if (((in == 0x08) || (in == 0x7f))&& i == 0) //if BS or DEL pressed, rubout sequence
            {
                i = 0;                                      //do nothing besides reset counter for more chars to be entered
                UART_PutChar(0x07);                         //put bel character if trying to backspace without inputting anything first
                inputArray[i] = '\0';   
            }
            else if (i >= (num_chars - 1))
            {
                inputArray[i] = '\0';
                UART_PutChar(0x07);                         //beep at user once desired num_chars writeable chars reached (num-1 since i starts at 0)
            }
            else
            {
                inputArray[i] = in;                         //save the data to the array
                UART_PutChar(in);                           //display to Realterm the character
                i++;
            }
        }
        else if (in == 0x0D)                                //if input enter, terminate string
        {
            enter_flag = 1;                                 //flag to begin parsing
            inputArray[i] = '\0';
            i = 100; //exit function           
        }
    }
}

uint16 get_address()
{
        memset(disp, 0, 5);
        save(disp,2);
        uint16 address = 0;
        switch(disp[0])                                     //starting address for SRAM SPI read/write
        {
            case '0': address = 0x0000;
                break;
            case '1': address = 0x2000;
                break;
            case '2': address = 0x4000;
                break;
            case '3': address = 0x6000;
                break;
            default: address = 0x0000;
                break;
        }
        return address;
}

void spi_write_block(uint16 starting_address,uint8* dataBuffer)   //starting address of which block to store the data in & the pointer to the RAM location of the 8192 bytes of data
{
    uint8 Buffer[NUM_BYTES];                                       //buffer must have write byte, upper & lower address bytes, & 8192 bytes worth of data
    Buffer[0] = 0x02;                                              //write command
    Buffer[1] = (starting_address & 0xFF00) >> 8;                  //get upper half of starting address
    Buffer[2] = starting_address & 0x00FF;                         //get lower half of starting address
    for(int i = 3; i < NUM_BYTES; i++) Buffer[i] = dataBuffer[i-3]; //assign rest of the buffer to the 8192 bytes of data from RAM
    
    hacked_SPIM_PutArray((const uint8*)Buffer,NUM_BYTES);
    while(!(SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE));              //wait until SPI transmission complete
}

void sequential_read_mode()                                         //enables sequential mode so don't have to do byte by byte reads
{
    uint8 Buffer[2];
    Buffer[0] = 0x01;                                               //status register write command
    Buffer[1] = 0x41;                                               //set sequential mode
    SPIM_PutArray(Buffer,2);                                        //write sequential mode in status register & hold disabled
    while(!(SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE));              //wait until SPI transmission complete
}

void spi_read_byte_block(uint16 starting_address,uint8* dataBuffer) //read one byte of data at a time from the starting address and store it in a global array from the SRAM to convert in DAC
{
    uint8 Buffer[4];
    Buffer[0] = 0x01;                                               //status register write command
    Buffer[1] = 0x41;                                               //set sequential mode
    SPIM_PutArray(Buffer,2);                                        //write sequential mode in status register & hold disabled
    while(!(SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE));              

    for(int j = 0; j < 8192; j++)
    {
        Buffer[0] = 0x03;                                           //read command
        Buffer[1] = (starting_address & 0xFF00) >> 8;               //starting address
        Buffer[2] = starting_address & 0x00FF;
        Buffer[3] = 0x00;                                           //dummy byte
    
        SPIM_ClearRxBuffer();
        SPIM_PutArray(Buffer,4);                                    //read and fill with dummy byte to force clock out
        while(!(SPIM_ReadTxStatus() & SPIM_STS_SPI_DONE));          //wait until SPI transmission complete
        
        for(int i = 1; i < 4;i++) SPIM_ReadRxData();                //readrxdata 3 times to remove 3 bytes and 4th time is data we want to store in buffer
        dataBuffer[j] = SPIM_ReadRxData();                          //store data in the global buffer array
        starting_address++;                                         //have to increment to the next address of the SRAM
    }
}

void parse()
{
    UART_PutString("\r\nType A or B to select the desired channel >");
    memset(disp, 0, 5);                                             //clear the array before input. only use 1 global array for all inputs by clearing every time
    save(disp,2);
    if ((disp[0] == 'A') || (disp[0] == 'a'))
    {
        UART_PutString("\r\nAddr A0: 0x0000 - 0x1FFF, 1: 0x2000 - 0x3FFF, 2: 0x4000-0x5FFF, 3: 0x6000-0x7FFF >");
        
        address_a = get_address();
        CH_SELECT_Select(0);                                        //select channel 0 on MUX for channel A 
        for(int i = 0; i < 8191; i++)
        {
            ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);               //ADC_GetResults32 is non-blocking so this is needed to wait for the conversion to finish
            ram[i] = ADC_GetResult8();                              //use this for more than just 1 sample at a time
        }                                                           //for loop for ADC 8192 conversions and save in a buffer array for the spi_write_block function
        spi_write_block(address_a,ram);                             //write the data to the SPI RAM
    }
    else if ((disp[0] == 'B') || (disp[0] == 'b'))
    {
        UART_PutString("\r\nAddr B 0: 0x0000 - 0x1FFF, 1: 0x2000 - 0x3FFF, 2: 0x4000-0x5FFF, 3: 0x6000-0x7FFF >");
        address_b = get_address();
        CH_SELECT_Select(1);                                        //select channel 1 on MUX for channel B
        for(int i = 0; i < 8191; i++)
        {
            ADC_IsEndConversion(ADC_WAIT_FOR_RESULT);               //ADC_GetResults32 is non-blocking so this is needed to wait for the conversion to finish
            ram[i] = ADC_GetResult8();                              //use this for more than just 1 sample at a time
        }                                                           //for loop for ADC 8192 conversions and save in a buffer array for the spi_write_block function
        spi_write_block(address_b,ram); 
    }
    else UART_PutString("\r\nInvalid Input");
}

uint16 convert_number(char* userInputArray)                          //requires 4 ascii numbers to be entered to convert to decimal number
{
    uint16 thousands = userInputArray[0] - 48;
    uint16 hundreds = userInputArray[1] - 48;
    uint16 tens = userInputArray[2] - 48;
    uint16 ones = userInputArray[3] - 48;
    uint16 number = (thousands * THOUSANDS_PLACE)+(hundreds*HUNDREDS_PLACE)+(tens*TENS_PLACE)+ones;
    if (number >= 8192) number = ERROR;                             //return error if greater than bounds
    return number;
}

void set_adc_clk()
{
    uint8 divider;
    switch(disp[0])                                                 //set divider for sample rate by changing clock divider
    {
        case '1': divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_1));
            break;
        case '2': divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_2));
            break;
        case '3': divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_3));
            break;
        case '4': divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_4));
            break;
        case '5': divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_5));
            break;
        default:  divider = (CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_1));
            break;
    }
    ADC_theACLK_SetDivider(divider);                                //sets the ADC clock to the desired sample rate
}

void get_dac_values() 
{
    DAC_UPDATE_ISR_Start();
    DAC8A_Start();
    DAC8B_Start();
    char input = 0;
    int i = 0;
    while (input == 0)                                              //wait for user to hit a key to exit after data is sent
    {
        input = UART_GetChar();
        if (flag)
        {
                DAC8A_SetValue(channel_a[i]);                       //outputs the analog conversion to the oscope probs to measure it
                DAC8B_SetValue(channel_b[i]);
                if (i == trigger) TRIGGER_Write(1);                 //if address is the same as trigger level then pull ext trigger to oscope high
                else if (i == 8192 || i == trigger+3) TRIGGER_Write(0);
                
                i = (i + 1) % 8192;                                 //modulus to reset the i to 0 so it keeps looping until user hits a button
                flag = 0;                                           //reset the flag for next dac sample to occur from interrupt
        }
    }
    DAC_UPDATE_ISR_Stop();                                          //don't need an interrupt occurring every clock cycle if dac function isn't being called
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    UART_Start();
    SPIM_Start();
    CH_SELECT_Start();
    ADC_Start();
    ADC_StartConvert();
    flag = 0;                                             
    ADC_theACLK_SetDivider(CLOCK_SPEED/(CLOCKS_PER_SAMPLE*RATE_3));         //default adc sample rate
    
    for(;;)
    {
        UART_PutString("\r\nType the prompted letter to issue a command.");
        UART_PutString("\r\nPress 1 for Sample, 2 for Playback, 3 for Change Rate, and 4 for Set Trigger >");
        
        userInput = blocking_char();
        if (userInput == '1')                           //Sample & write to SPI RAM
        {
            sequential_read_mode();                     //make sure sequential read mode is enabled
            parse();
        }
        else if (userInput == '2')                      //playback SPI RAM to DAC
        {
            UART_PutString("\r\nWhich RAM block for Channel A? (0: 0x0000 - 0x1FFF, 1: 0x2000 - 0x3FFF, 2: 0x4000-0x5FFF, 3: 0x6000-0x7FFF >");
            address_a = get_address();                  //gets starting address for spi read on channel A
            spi_read_byte_block(address_a,channel_a);   //channel_a 8k array populated with data to send to DAC
            
            UART_PutString("\r\nWhich RAM block for Channel B? (0: 0x0000 - 0x1FFF, 1: 0x2000 - 0x3FFF, 2: 0x4000-0x5FFF, 3: 0x6000-0x7FFF >");
            address_b = get_address();
            spi_read_byte_block(address_b,channel_b);
            
            get_dac_values();                           //output analog values of A & B to oscope at selected sample rate & toggle external trigger if needed
        }
        else if (userInput == '3')                      //change rate
        {
            UART_PutString("\r\nSelect the sample/playback rate (ksps) 1: 50, 2: 75, 3: 100, 4: 125, 5: 150 >");
            memset(disp, 0, 5);
            save(disp,2);
            set_adc_clk();
        }
        else if (userInput == '4')                      //trigger level
        {
            UART_PutString("\r\nInput a number between 0-8191 (type 4 characters): >");
            memset(disp, 0, 5);
            save(disp,5);
            trigger = convert_number(disp);                                                  //parse number to compute where trigger should be
            if (trigger == ERROR) UART_PutString("\r\nInvalid Input");                       //error checking
        }       
        else UART_PutString("\r\nInvalid Input"); //invalid input if the character is something other than '1' through '4'     
    }
}
/* [] END OF FILE */
