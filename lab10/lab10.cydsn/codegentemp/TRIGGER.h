/*******************************************************************************
* File Name: TRIGGER.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_TRIGGER_H) /* Pins TRIGGER_H */
#define CY_PINS_TRIGGER_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "TRIGGER_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 TRIGGER__PORT == 15 && ((TRIGGER__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    TRIGGER_Write(uint8 value);
void    TRIGGER_SetDriveMode(uint8 mode);
uint8   TRIGGER_ReadDataReg(void);
uint8   TRIGGER_Read(void);
void    TRIGGER_SetInterruptMode(uint16 position, uint16 mode);
uint8   TRIGGER_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the TRIGGER_SetDriveMode() function.
     *  @{
     */
        #define TRIGGER_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define TRIGGER_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define TRIGGER_DM_RES_UP          PIN_DM_RES_UP
        #define TRIGGER_DM_RES_DWN         PIN_DM_RES_DWN
        #define TRIGGER_DM_OD_LO           PIN_DM_OD_LO
        #define TRIGGER_DM_OD_HI           PIN_DM_OD_HI
        #define TRIGGER_DM_STRONG          PIN_DM_STRONG
        #define TRIGGER_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define TRIGGER_MASK               TRIGGER__MASK
#define TRIGGER_SHIFT              TRIGGER__SHIFT
#define TRIGGER_WIDTH              1u

/* Interrupt constants */
#if defined(TRIGGER__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in TRIGGER_SetInterruptMode() function.
     *  @{
     */
        #define TRIGGER_INTR_NONE      (uint16)(0x0000u)
        #define TRIGGER_INTR_RISING    (uint16)(0x0001u)
        #define TRIGGER_INTR_FALLING   (uint16)(0x0002u)
        #define TRIGGER_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define TRIGGER_INTR_MASK      (0x01u) 
#endif /* (TRIGGER__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define TRIGGER_PS                     (* (reg8 *) TRIGGER__PS)
/* Data Register */
#define TRIGGER_DR                     (* (reg8 *) TRIGGER__DR)
/* Port Number */
#define TRIGGER_PRT_NUM                (* (reg8 *) TRIGGER__PRT) 
/* Connect to Analog Globals */                                                  
#define TRIGGER_AG                     (* (reg8 *) TRIGGER__AG)                       
/* Analog MUX bux enable */
#define TRIGGER_AMUX                   (* (reg8 *) TRIGGER__AMUX) 
/* Bidirectional Enable */                                                        
#define TRIGGER_BIE                    (* (reg8 *) TRIGGER__BIE)
/* Bit-mask for Aliased Register Access */
#define TRIGGER_BIT_MASK               (* (reg8 *) TRIGGER__BIT_MASK)
/* Bypass Enable */
#define TRIGGER_BYP                    (* (reg8 *) TRIGGER__BYP)
/* Port wide control signals */                                                   
#define TRIGGER_CTL                    (* (reg8 *) TRIGGER__CTL)
/* Drive Modes */
#define TRIGGER_DM0                    (* (reg8 *) TRIGGER__DM0) 
#define TRIGGER_DM1                    (* (reg8 *) TRIGGER__DM1)
#define TRIGGER_DM2                    (* (reg8 *) TRIGGER__DM2) 
/* Input Buffer Disable Override */
#define TRIGGER_INP_DIS                (* (reg8 *) TRIGGER__INP_DIS)
/* LCD Common or Segment Drive */
#define TRIGGER_LCD_COM_SEG            (* (reg8 *) TRIGGER__LCD_COM_SEG)
/* Enable Segment LCD */
#define TRIGGER_LCD_EN                 (* (reg8 *) TRIGGER__LCD_EN)
/* Slew Rate Control */
#define TRIGGER_SLW                    (* (reg8 *) TRIGGER__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define TRIGGER_PRTDSI__CAPS_SEL       (* (reg8 *) TRIGGER__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define TRIGGER_PRTDSI__DBL_SYNC_IN    (* (reg8 *) TRIGGER__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define TRIGGER_PRTDSI__OE_SEL0        (* (reg8 *) TRIGGER__PRTDSI__OE_SEL0) 
#define TRIGGER_PRTDSI__OE_SEL1        (* (reg8 *) TRIGGER__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define TRIGGER_PRTDSI__OUT_SEL0       (* (reg8 *) TRIGGER__PRTDSI__OUT_SEL0) 
#define TRIGGER_PRTDSI__OUT_SEL1       (* (reg8 *) TRIGGER__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define TRIGGER_PRTDSI__SYNC_OUT       (* (reg8 *) TRIGGER__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(TRIGGER__SIO_CFG)
    #define TRIGGER_SIO_HYST_EN        (* (reg8 *) TRIGGER__SIO_HYST_EN)
    #define TRIGGER_SIO_REG_HIFREQ     (* (reg8 *) TRIGGER__SIO_REG_HIFREQ)
    #define TRIGGER_SIO_CFG            (* (reg8 *) TRIGGER__SIO_CFG)
    #define TRIGGER_SIO_DIFF           (* (reg8 *) TRIGGER__SIO_DIFF)
#endif /* (TRIGGER__SIO_CFG) */

/* Interrupt Registers */
#if defined(TRIGGER__INTSTAT)
    #define TRIGGER_INTSTAT            (* (reg8 *) TRIGGER__INTSTAT)
    #define TRIGGER_SNAP               (* (reg8 *) TRIGGER__SNAP)
    
	#define TRIGGER_0_INTTYPE_REG 		(* (reg8 *) TRIGGER__0__INTTYPE)
#endif /* (TRIGGER__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_TRIGGER_H */


/* [] END OF FILE */
