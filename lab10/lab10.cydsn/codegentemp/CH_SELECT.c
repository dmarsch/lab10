/*******************************************************************************
* File Name: CH_SELECT.c
* Version 1.80
*
*  Description:
*    This file contains all functions required for the analog multiplexer
*    AMux User Module.
*
*   Note:
*
*******************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "CH_SELECT.h"

static uint8 CH_SELECT_lastChannel = CH_SELECT_NULL_CHANNEL;


/*******************************************************************************
* Function Name: CH_SELECT_Start
********************************************************************************
* Summary:
*  Disconnect all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_Start(void) 
{
    uint8 chan;

    for(chan = 0u; chan < CH_SELECT_CHANNELS ; chan++)
    {
#if (CH_SELECT_MUXTYPE == CH_SELECT_MUX_SINGLE)
        CH_SELECT_Unset(chan);
#else
        CH_SELECT_CYAMUXSIDE_A_Unset(chan);
        CH_SELECT_CYAMUXSIDE_B_Unset(chan);
#endif
    }

    CH_SELECT_lastChannel = CH_SELECT_NULL_CHANNEL;
}


#if (!CH_SELECT_ATMOSTONE)
/*******************************************************************************
* Function Name: CH_SELECT_Select
********************************************************************************
* Summary:
*  This functions first disconnects all channels then connects the given
*  channel.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_Select(uint8 channel) 
{
    CH_SELECT_DisconnectAll();        /* Disconnect all previous connections */
    CH_SELECT_Connect(channel);       /* Make the given selection */
    CH_SELECT_lastChannel = channel;  /* Update last channel */
}
#endif


/*******************************************************************************
* Function Name: CH_SELECT_FastSelect
********************************************************************************
* Summary:
*  This function first disconnects the last connection made with FastSelect or
*  Select, then connects the given channel. The FastSelect function is similar
*  to the Select function, except it is faster since it only disconnects the
*  last channel selected rather than all channels.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_FastSelect(uint8 channel) 
{
    /* Disconnect the last valid channel */
    if( CH_SELECT_lastChannel != CH_SELECT_NULL_CHANNEL)
    {
        CH_SELECT_Disconnect(CH_SELECT_lastChannel);
    }

    /* Make the new channel connection */
#if (CH_SELECT_MUXTYPE == CH_SELECT_MUX_SINGLE)
    CH_SELECT_Set(channel);
#else
    CH_SELECT_CYAMUXSIDE_A_Set(channel);
    CH_SELECT_CYAMUXSIDE_B_Set(channel);
#endif


    CH_SELECT_lastChannel = channel;   /* Update last channel */
}


#if (CH_SELECT_MUXTYPE == CH_SELECT_MUX_DIFF)
#if (!CH_SELECT_ATMOSTONE)
/*******************************************************************************
* Function Name: CH_SELECT_Connect
********************************************************************************
* Summary:
*  This function connects the given channel without affecting other connections.
*
* Parameters:
*  channel:  The channel to connect to the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_Connect(uint8 channel) 
{
    CH_SELECT_CYAMUXSIDE_A_Set(channel);
    CH_SELECT_CYAMUXSIDE_B_Set(channel);
}
#endif

/*******************************************************************************
* Function Name: CH_SELECT_Disconnect
********************************************************************************
* Summary:
*  This function disconnects the given channel from the common or output
*  terminal without affecting other connections.
*
* Parameters:
*  channel:  The channel to disconnect from the common terminal.
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_Disconnect(uint8 channel) 
{
    CH_SELECT_CYAMUXSIDE_A_Unset(channel);
    CH_SELECT_CYAMUXSIDE_B_Unset(channel);
}
#endif

#if (CH_SELECT_ATMOSTONE)
/*******************************************************************************
* Function Name: CH_SELECT_DisconnectAll
********************************************************************************
* Summary:
*  This function disconnects all channels.
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void CH_SELECT_DisconnectAll(void) 
{
    if(CH_SELECT_lastChannel != CH_SELECT_NULL_CHANNEL) 
    {
        CH_SELECT_Disconnect(CH_SELECT_lastChannel);
        CH_SELECT_lastChannel = CH_SELECT_NULL_CHANNEL;
    }
}
#endif

/* [] END OF FILE */
