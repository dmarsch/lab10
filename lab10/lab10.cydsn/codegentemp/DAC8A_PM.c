/*******************************************************************************
* File Name: DAC8A_PM.c  
* Version 1.90
*
* Description:
*  This file provides the power management source code to API for the
*  VDAC8.  
*
* Note:
*  None
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "DAC8A.h"

static DAC8A_backupStruct DAC8A_backup;


/*******************************************************************************
* Function Name: DAC8A_SaveConfig
********************************************************************************
* Summary:
*  Save the current user configuration
*
* Parameters:  
*  void  
*
* Return: 
*  void
*
*******************************************************************************/
void DAC8A_SaveConfig(void) 
{
    if (!((DAC8A_CR1 & DAC8A_SRC_MASK) == DAC8A_SRC_UDB))
    {
        DAC8A_backup.data_value = DAC8A_Data;
    }
}


/*******************************************************************************
* Function Name: DAC8A_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the current user configuration.
*
* Parameters:  
*  void
*
* Return: 
*  void
*
*******************************************************************************/
void DAC8A_RestoreConfig(void) 
{
    if (!((DAC8A_CR1 & DAC8A_SRC_MASK) == DAC8A_SRC_UDB))
    {
        if((DAC8A_Strobe & DAC8A_STRB_MASK) == DAC8A_STRB_EN)
        {
            DAC8A_Strobe &= (uint8)(~DAC8A_STRB_MASK);
            DAC8A_Data = DAC8A_backup.data_value;
            DAC8A_Strobe |= DAC8A_STRB_EN;
        }
        else
        {
            DAC8A_Data = DAC8A_backup.data_value;
        }
    }
}


/*******************************************************************************
* Function Name: DAC8A_Sleep
********************************************************************************
* Summary:
*  Stop and Save the user configuration
*
* Parameters:  
*  void:  
*
* Return: 
*  void
*
* Global variables:
*  DAC8A_backup.enableState:  Is modified depending on the enable 
*  state  of the block before entering sleep mode.
*
*******************************************************************************/
void DAC8A_Sleep(void) 
{
    /* Save VDAC8's enable state */    
    if(DAC8A_ACT_PWR_EN == (DAC8A_PWRMGR & DAC8A_ACT_PWR_EN))
    {
        /* VDAC8 is enabled */
        DAC8A_backup.enableState = 1u;
    }
    else
    {
        /* VDAC8 is disabled */
        DAC8A_backup.enableState = 0u;
    }
    
    DAC8A_Stop();
    DAC8A_SaveConfig();
}


/*******************************************************************************
* Function Name: DAC8A_Wakeup
********************************************************************************
*
* Summary:
*  Restores and enables the user configuration
*  
* Parameters:  
*  void
*
* Return: 
*  void
*
* Global variables:
*  DAC8A_backup.enableState:  Is used to restore the enable state of 
*  block on wakeup from sleep mode.
*
*******************************************************************************/
void DAC8A_Wakeup(void) 
{
    DAC8A_RestoreConfig();
    
    if(DAC8A_backup.enableState == 1u)
    {
        /* Enable VDAC8's operation */
        DAC8A_Enable();

        /* Restore the data register */
        DAC8A_SetValue(DAC8A_Data);
    } /* Do nothing if VDAC8 was disabled before */    
}


/* [] END OF FILE */
