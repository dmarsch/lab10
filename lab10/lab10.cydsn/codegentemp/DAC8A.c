/*******************************************************************************
* File Name: DAC8A.c  
* Version 1.90
*
* Description:
*  This file provides the source code to the API for the 8-bit Voltage DAC 
*  (VDAC8) User Module.
*
* Note:
*  Any unusual or non-standard behavior should be noted here. Other-
*  wise, this section should remain blank.
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "cytypes.h"
#include "DAC8A.h"

#if (CY_PSOC5A)
#include <CyLib.h>
#endif /* CY_PSOC5A */

uint8 DAC8A_initVar = 0u;

#if (CY_PSOC5A)
    static uint8 DAC8A_restoreVal = 0u;
#endif /* CY_PSOC5A */

#if (CY_PSOC5A)
    static DAC8A_backupStruct DAC8A_backup;
#endif /* CY_PSOC5A */


/*******************************************************************************
* Function Name: DAC8A_Init
********************************************************************************
* Summary:
*  Initialize to the schematic state.
* 
* Parameters:
*  void:
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_Init(void) 
{
    DAC8A_CR0 = (DAC8A_MODE_V );

    /* Set default data source */
    #if(DAC8A_DEFAULT_DATA_SRC != 0 )
        DAC8A_CR1 = (DAC8A_DEFAULT_CNTL | DAC8A_DACBUS_ENABLE) ;
    #else
        DAC8A_CR1 = (DAC8A_DEFAULT_CNTL | DAC8A_DACBUS_DISABLE) ;
    #endif /* (DAC8A_DEFAULT_DATA_SRC != 0 ) */

    /* Set default strobe mode */
    #if(DAC8A_DEFAULT_STRB != 0)
        DAC8A_Strobe |= DAC8A_STRB_EN ;
    #endif/* (DAC8A_DEFAULT_STRB != 0) */

    /* Set default range */
    DAC8A_SetRange(DAC8A_DEFAULT_RANGE); 

    /* Set default speed */
    DAC8A_SetSpeed(DAC8A_DEFAULT_SPEED);
}


/*******************************************************************************
* Function Name: DAC8A_Enable
********************************************************************************
* Summary:
*  Enable the VDAC8
* 
* Parameters:
*  void
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_Enable(void) 
{
    DAC8A_PWRMGR |= DAC8A_ACT_PWR_EN;
    DAC8A_STBY_PWRMGR |= DAC8A_STBY_PWR_EN;

    /*This is to restore the value of register CR0 ,
    which is modified  in Stop API , this prevents misbehaviour of VDAC */
    #if (CY_PSOC5A)
        if(DAC8A_restoreVal == 1u) 
        {
             DAC8A_CR0 = DAC8A_backup.data_value;
             DAC8A_restoreVal = 0u;
        }
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: DAC8A_Start
********************************************************************************
*
* Summary:
*  The start function initializes the voltage DAC with the default values, 
*  and sets the power to the given level.  A power level of 0, is the same as
*  executing the stop function.
*
* Parameters:
*  Power: Sets power level between off (0) and (3) high power
*
* Return:
*  void 
*
* Global variables:
*  DAC8A_initVar: Is modified when this function is called for the 
*  first time. Is used to ensure that initialization happens only once.
*
*******************************************************************************/
void DAC8A_Start(void)  
{
    /* Hardware initiazation only needs to occure the first time */
    if(DAC8A_initVar == 0u)
    { 
        DAC8A_Init();
        DAC8A_initVar = 1u;
    }

    /* Enable power to DAC */
    DAC8A_Enable();

    /* Set default value */
    DAC8A_SetValue(DAC8A_DEFAULT_DATA); 
}


/*******************************************************************************
* Function Name: DAC8A_Stop
********************************************************************************
*
* Summary:
*  Powers down DAC to lowest power state.
*
* Parameters:
*  void
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_Stop(void) 
{
    /* Disble power to DAC */
    DAC8A_PWRMGR &= (uint8)(~DAC8A_ACT_PWR_EN);
    DAC8A_STBY_PWRMGR &= (uint8)(~DAC8A_STBY_PWR_EN);

    /* This is a work around for PSoC5A  ,
    this sets VDAC to current mode with output off */
    #if (CY_PSOC5A)
        DAC8A_backup.data_value = DAC8A_CR0;
        DAC8A_CR0 = DAC8A_CUR_MODE_OUT_OFF;
        DAC8A_restoreVal = 1u;
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: DAC8A_SetSpeed
********************************************************************************
*
* Summary:
*  Set DAC speed
*
* Parameters:
*  power: Sets speed value
*
* Return:
*  void
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_SetSpeed(uint8 speed) 
{
    /* Clear power mask then write in new value */
    DAC8A_CR0 &= (uint8)(~DAC8A_HS_MASK);
    DAC8A_CR0 |=  (speed & DAC8A_HS_MASK);
}


/*******************************************************************************
* Function Name: DAC8A_SetRange
********************************************************************************
*
* Summary:
*  Set one of three current ranges.
*
* Parameters:
*  Range: Sets one of Three valid ranges.
*
* Return:
*  void 
*
* Theory:
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_SetRange(uint8 range) 
{
    DAC8A_CR0 &= (uint8)(~DAC8A_RANGE_MASK);      /* Clear existing mode */
    DAC8A_CR0 |= (range & DAC8A_RANGE_MASK);      /*  Set Range  */
    DAC8A_DacTrim();
}


/*******************************************************************************
* Function Name: DAC8A_SetValue
********************************************************************************
*
* Summary:
*  Set 8-bit DAC value
*
* Parameters:  
*  value:  Sets DAC value between 0 and 255.
*
* Return: 
*  void 
*
* Theory: 
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_SetValue(uint8 value) 
{
    #if (CY_PSOC5A)
        uint8 DAC8A_intrStatus = CyEnterCriticalSection();
    #endif /* CY_PSOC5A */

    DAC8A_Data = value;                /*  Set Value  */

    /* PSOC5A requires a double write */
    /* Exit Critical Section */
    #if (CY_PSOC5A)
        DAC8A_Data = value;
        CyExitCriticalSection(DAC8A_intrStatus);
    #endif /* CY_PSOC5A */
}


/*******************************************************************************
* Function Name: DAC8A_DacTrim
********************************************************************************
*
* Summary:
*  Set the trim value for the given range.
*
* Parameters:
*  range:  1V or 4V range.  See constants.
*
* Return:
*  void
*
* Theory: 
*
* Side Effects:
*
*******************************************************************************/
void DAC8A_DacTrim(void) 
{
    uint8 mode;

    mode = (uint8)((DAC8A_CR0 & DAC8A_RANGE_MASK) >> 2) + DAC8A_TRIM_M7_1V_RNG_OFFSET;
    DAC8A_TR = CY_GET_XTND_REG8((uint8 *)(DAC8A_DAC_TRIM_BASE + mode));
}


/* [] END OF FILE */
